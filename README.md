-----------------------------------
Steps to prepare files for the day
-----------------------------------

## Prepare Server folder

## Prepare Client Folder

## Prepare Git 
1. Initialize git on folder ```git init``` 
2. Create remote repository in bitbucket (Plus sign on bitbucket)
3. Add remote add origin by pasting over code ```git remote add origin (http://bitbucketlink here)```
4. Git add to staging area ```git add .```
5. Git commit to local repository ```git commit -m "message here"```
6. Git push to remote repository ```git push origin master -u```

## Prepare Dependencies
1. Prepare scafolding aka package.JSON file ```npm init```
2. Download and install express ```npm install express --save```
3. Download and install body-parser ```npm install body-parser --save```

## Bower 
1 Download bower globally with -g (This handles front end modules and dependencies) ```npm install -g bower```
2. Create a .bowerrc file in the root directory to point to the client folder for installing front-end modules (refer to file for path) ```touch .bowerrc```
3. Download and install Angular ```bower install angular --save```
4. Download and install Bootstrap ```bower install bootstrap --save```
5. Download and install Font Awesome ```bower install font-awesome --save```

## Starting the app (Console)
1. ```export NODE_PORT=3000```
2. ```nodemon``` or ```nodemon server/app.js```

## Launching the app on the browser
1. ```http://localhost:3000```